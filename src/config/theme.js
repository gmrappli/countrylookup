import {createTheme, Theme, makeStyles, ThemeProvider} from '@material-ui/core/styles';

const defaultTheme = createTheme();
const theme = createTheme({
  palette: {
    primary: {
      main: '#8362F2'
    },
    secondary: {
      main: '#8DD4CC',
      contrastText: '#FFFFFF'
    }
  },
  typography: {
    fontSize: 20,
    h1: {
      fontSize: '3.75em',
      fontWeight: 'bold',
    },
    h2: {
      fontSize: '2.5em',
    },
    h3: {
      fontSize: '1.5em',
    },
    body: {
      fontSize: '1.5em'
    },
    subtitle1: {
      fontSize: '0.8em'
    }
  },
  overrides: {
    MuiInputBase: {
      'root': {
        '&.MuiOutlinedInput-root': {
          borderRadius: '0.5em',
        },
        '&$focused .MuiSvgIcon-root': {
          color: '#8362f2',
        },
        '&$error .MuiSvgIcon-root': {
          color: defaultTheme.palette.error.main,
        },
      },
    },
    MuiButton: {
      text: {
        color: 'white',
      },
    },
  },
});

export default theme;
