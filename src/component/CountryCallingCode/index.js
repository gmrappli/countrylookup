import React, {useState} from 'react';
import SectionTitle from "../SectionTitle";
import {Box, Link, makeStyles, Paper, Typography} from "@material-ui/core";
import CountryPopover from "../CountryPopover";

const useStyles = makeStyles({
  popOverLink: {
    display: 'inline-block',
    cursor: 'pointer'
  }
});

function CountryCallingCode(props) {
  const {data, ...paperProps} = props;
  const classes = useStyles();

  const [popoverData, setPopoverData] = useState();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const popData = (open, data) => (e) => {
    setPopoverData(open ? data : null);
    setAnchorEl(open ? e.currentTarget : null);
  };

  return (
    <Paper  {...paperProps}>
      <SectionTitle>
        Calling Code
      </SectionTitle>

      {
        Array.from(data?.keys()).map((v, idx) => {
          const callcodeData = data.get(v);

          return (
            <Box key={idx}>
              <Typography variant={"h2"} component={'p'} color={'primary'}>
                <b>{v}</b>
              </Typography>

              <Typography variant={'subtitle1'}>
                <Link
                  onMouseEnter={popData(true, callcodeData)}
                  onMouseLeave={popData(false)}
                  underline={'always'}
                  className={classes.popOverLink}
                >
                  <b>
                    {callcodeData?.length} {callcodeData?.length > 1 ? ' countries' : ` country`}
                  </b>

                  <CountryPopover
                    id={'callcode-popover'}
                    open={Boolean(anchorEl)}
                    anchorEl={anchorEl}
                    onClose={popData(false)}
                    hideOnHoverOut>
                    {
                      popoverData?.map((v) => {
                        return (
                          <Typography variant={'subtitle1'}>
                            {v?.name}
                          </Typography>
                        )
                      })
                    }
                  </CountryPopover>
                </Link>

                {' '}
                <b>
                  with this calling code
                </b>
              </Typography>


            </Box>
          )
        })
      }
    </Paper>
  );
}

CountryCallingCode.propTypes = {};

export default CountryCallingCode;
