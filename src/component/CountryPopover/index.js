import React from 'react';
import {makeStyles, Popover, Typography} from "@material-ui/core";

const usePopStyles = makeStyles(() => {
  return {
    root: {},
    paper: {
      background: '#525252',
      color: 'white',
      borderRadius: '0.75em',
      padding: '1em',
    }
  }
});

function CountryPopover(props) {
  const popClasses = usePopStyles();
  const {onClose, hideOnHoverOut, children, ...otherProps} = props;

  return (
    <Popover
      onClose={onClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      classes={popClasses}
      {...otherProps}
      PaperProps={{
        ...otherProps?.PaperProps,
        onMouseLeave: hideOnHoverOut ? onClose : undefined
      }}
    >
      {children}
    </Popover>
  );
}

CountryPopover.propTypes = {};

export default CountryPopover;
