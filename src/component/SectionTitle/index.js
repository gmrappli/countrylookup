import React from "react";
import {Typography} from "@material-ui/core";

const SectionTitle = (props) => {
  const {children, ...otherProps} = props;
  return (
    <Typography
      variant={'body2'}
      component={'h1'}
      style={{fontWeight: "bold", marginBottom: '0.75em'}}
      {...otherProps}
    >
      {children}
    </Typography>
  )
};

export default SectionTitle;
