import React, {useState} from 'react';
import {Link as RouterLink, useParams} from "react-router-dom";
import axios from "axios";
import {Box, Button, Chip, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import ArrowLeft from '@material-ui/icons/ArrowBack';
import SectionTitle from "../component/SectionTitle";
import CountryCallingCode from "../component/CountryCallingCode";
import CountryCurrency from "../component/CountryCurrency";

const useStyles = makeStyles({
  chipStyle: {
    fontSize: '0.625em',
    fontWeight: 'bold',
    marginRight: '0.5em',
    marginBottom: '0.5em',
    padding: '0.45em',
    height: 'auto'
  },
  homepageBtnStyle: {
    fontSize: '0.95em',
    backgroundColor: 'primary',
    marginBottom: '3.25em',
    borderRadius: 8,
    textTransform: 'initial',
    fontWeight: 'normal'
  },
  homepageBtnIconStyle: {
    width: '1.25em', height: '1.25em'
  },
  countryFlagStyle: {height: '0.6em', marginLeft: '0.25em'},
  paperElevatedStyle: {padding: '1em 1.5em', height: '100%'},
  paperStyle: {padding: '1em 1.5em', height: '100%'}
});

function Country() {
  const params = useParams();
  const classes = useStyles();
  const [country, setCountry] = useState();
  const [currency, setCurrency] = useState(new Map());
  const [callcode, setCallcode] = useState(new Map());

  React.useEffect(() => {
    const {id} = params;
    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();

    if (id) {
      axios.get(
        `https://restcountries.eu/rest/v2/name/${id}?fullText=true`, {
          cancelToken: source.token,
        }
      )
        .then((res) => {
          const {data} = res;

          setCountry(data && data[0]);
        })
        .catch(() => {

        });
    }

    return () => {
      source.cancel();
    };
  }, [params]);

  React.useEffect(() => {
    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();

    if (country?.callingCodes) {
      // get all calling codes detail of this country
      const apiPaths = country?.callingCodes?.map(v => {
        return `https://restcountries.eu/rest/v2/callingcode/${v}`;
      });

      apiPaths.forEach((p, idx) => {
        axios.get(p, {
          cancelToken: source.token,
        })
          .then((res) => {
            setCallcode((d) => {
              const newData = new Map(d);
              newData.set(country?.callingCodes[idx], res.data);
              return newData;
            });
          })
          .catch(() => {

          })
      });

    }

    if (country?.currencies) {
      // get all currencies detail of this country
      const apiPaths = country?.currencies?.map(v => {
        return `https://restcountries.eu/rest/v2/currency/${v.code}`;
      });

      apiPaths.forEach((p, idx) => {
        axios.get(p, {
          cancelToken: source.token,
        })
          .then((res) => {
            setCurrency((d) => {
              const newData = new Map(d);
              newData.set(country?.currencies[idx], res.data);
              return newData;
            });
          })
          .catch(() => {

          })
      });
    }

    return () => {
      source.cancel();
    }
  }, [country]);

  return (
    <Box paddingTop={'4.75em'} paddingLeft={'6.5%'} paddingRight={'15%'} paddingBottom={'5em'}>
      <Button
        component={RouterLink}
        to={'/'}
        variant={'contained'}
        color={'primary'}
        className={classes.homepageBtnStyle}
        size={'large'}
        startIcon={<ArrowLeft className={classes.homepageBtnIconStyle} />}>
        Back to Homepage
      </Button>

      <Typography variant={"h2"} component={"h1"}>
        <b>{country?.name}</b>

        <img src={country?.flag} alt={country?.name} className={classes.countryFlagStyle} />
      </Typography>

      <Box marginBottom={'1em'}>
        {
          country?.altSpellings?.map(v => {
            return (
              <Chip
                color={'secondary'}
                key={v}
                label={v}
                size={'medium'}
                className={classes.chipStyle}
              />
            )
          })
        }
      </Box>

      <Grid container spacing={3}>
        <Grid item xs={6} component={'section'}>
          <Paper elevation={3} className={classes.paperElevatedStyle}
                 style={{
                   background: 'url("/globe.svg") right -20%',
                   backgroundSize: '10em auto',
                   backgroundRepeat: 'no-repeat'
                 }}>
            <SectionTitle>
              LatLong
            </SectionTitle>

            <Typography variant={"h2"} component={'p'} style={{fontWeight: "bold"}} color={'primary'}>
              {country?.latlng?.map(v => v?.toFixed(1)).join(', ')}
            </Typography>
          </Paper>
        </Grid>

        <Grid item xs={6} component={'section'}>
          <Paper elevation={3} className={classes.paperElevatedStyle}>
            <div>Capital : <b>{country?.capital}</b></div>
            <div>Region: <b>{country?.region}</b></div>
            <div>Subregion: <b>{country?.subregion}</b></div>
          </Paper>
        </Grid>

        <Grid item xs={6} component={'section'}>
          <CountryCallingCode
            data={callcode}
            elevation={0}
            className={classes.paperStyle}
          />
        </Grid>

        <Grid item xs={6} component={'section'}>
          <CountryCurrency
            data={currency}
            elevation={0}
            className={classes.paperStyle}
          />
        </Grid>
      </Grid>
    </Box>
  );
}

Country.propTypes = {};

export default Country;
