import React, {useState} from 'react';
import {Box, TextField, Typography} from "@material-ui/core";
import {Autocomplete} from "@material-ui/lab";
import {Search} from '@material-ui/icons';
import axios from 'axios';

function Home() {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [searchKey, setSearchKey] = useState();
  const [result, setResult] = useState([]);

  React.useEffect(() => {
    if (!open) {
      setResult([]);
    }
  }, [open]);

  React.useEffect(() => {
    setLoading(true);

    if (!searchKey?.length) {
      setResult([]);
      return undefined;
    }

    const notFoundEntry = {id: -1, name: `Data not found`};
    const debouncedFetch = setTimeout(() => {
      const req = axios.get(`https://restcountries.eu/rest/v2/name/${searchKey ?? ''}`);

      req
        .then((response) => {
          const {data} = response;

          let res = data?.slice(0, 5) || [notFoundEntry];

          setResult(res);
        })
        .catch(() => {
          setResult([notFoundEntry]);
        })
        .finally(() => {
          setLoading(false);
        });

    }, 500);


    return () => {
      clearTimeout(debouncedFetch);
    };
  }, [searchKey]);

  return (
    <Box width={'100%'} height={'100%'} display={'flex'} alignItems={'center'} justifyContent={'center'}
         flexDirection={'column'}>
      <Typography variant={'h1'} style={{marginBottom: '0.5em'}}>Country</Typography>

      <Box width={'48%'}>
        <Autocomplete
          id="field-country"
          freeSolo
          debug
          fullWidth
          disableClearable
          options={result}
          open={open}
          onChange={(event, newValue) => {
            window.location.href = `/country/${newValue?.name}`
          }}
          onOpen={() => {
            setOpen(true);
          }}
          onClose={() => {
            setOpen(false);
          }}
          autoHighlight
          getOptionLabel={(option) => option?.id === -1 ? searchKey : option.name}
          getOptionDisabled={(option) => option.id === -1}
          renderOption={(option) => {
            return (
              <span style={{color: option.id === -1 ? '#FF0000' : undefined}}>
              {option.name}
            </span>
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              placeholder="Type any country name"
              variant="outlined"
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    <Search />
                  </React.Fragment>
                ),
              }}
              inputProps={{
                ...params.inputProps,
                onChange: (event) => {
                  setSearchKey(event.target.value);
                  params.inputProps.onChange(event);
                },
              }}
            />
          )}
        />
      </Box>
    </Box>
  );
}

Home.propTypes = {};

export default Home;
