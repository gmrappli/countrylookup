# Country Lookup

This project is made as part of a job application test. 


## How To Run

Before running, make sure the dependencies are installed first.

Either run `yarn` or `npm` in the project. This project provide both `yarn.lock` and `package.lock`.


#### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
